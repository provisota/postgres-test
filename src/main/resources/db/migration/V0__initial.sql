create schema if not exists test_schema;

create table first_entity
(
    id             uuid
        constraint first_entity_pk
            primary key,
    string_column  varchar not null,
    int_column     int     not null,
    boolean_column bool    not null
);
