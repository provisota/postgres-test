package com.example.postgres.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "first_entity", schema = "test_schema")
data class FirstEntity(
    @Id
    @Column(name = "id")
    val id: UUID = UUID.randomUUID(),

    @Column(name = "string_column")
    val stringColumn: String,

    @Column(name = "int_column")
    val intColumn: Int,

    @Column(name = "boolean_column")
    var booleanColumn: Boolean
)