package com.example.postgres

import com.example.postgres.entity.FirstEntity
import com.example.postgres.repository.FirstEntityRepository
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import java.util.*
import kotlin.random.Random

@Component
class ApplicationStartup(
    private val firstEntityRepository: FirstEntityRepository
) : ApplicationListener<ApplicationReadyEvent> {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        firstEntityRepository.deleteAll()

        for (i in 1..10) {
            firstEntityRepository.save(
                FirstEntity(
                    stringColumn = "Random_String_${UUID.randomUUID()}",
                    intColumn = i,
                    booleanColumn = Random.nextBoolean()
                )
            )
        }
    }
}