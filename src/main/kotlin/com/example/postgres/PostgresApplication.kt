package com.example.postgres

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationPropertiesScan(basePackages = ["com.example.postgres"])
@EntityScan(value = ["com.example.postgres"])
@EnableJpaRepositories(basePackages = ["com.example.postgres"])
class PostgresApplication

fun main(args: Array<String>) {
    runApplication<PostgresApplication>(*args)
}
