package com.example.postgres.repository

import com.example.postgres.entity.FirstEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface FirstEntityRepository : CrudRepository<FirstEntity, String>
